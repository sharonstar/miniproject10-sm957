use warp::Filter;
use serde::{Deserialize, Serialize};
use warp::http::StatusCode;

#[derive(Deserialize)]
struct Request {
    input: String,
}

#[derive(Serialize)]
struct Response {
    res: String,
}


#[tokio::main]
async fn main() {
    // POST /transformer endpoint
    let transformer_route = warp::path("transformer")
        .and(warp::post())
        .and(warp::body::json())
        .and_then(transformer_handler);

    // Run the server
    warp::serve(transformer_route)
        .run(([0, 0, 0, 0], 8080))
        .await;
}

async fn transformer_handler(body: Request) -> Result<impl warp::Reply, warp::Rejection> {
    let response = Response {
        res: format!("Output: {}", body.input),
    };
    Ok(warp::reply::with_status(
        warp::reply::json(&response),
        StatusCode::OK,
    ))
}

