# miniProject10-sm957



###  Requirements
Rust Serverless Transformer Endpoint

- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

###  Steps

1. Create a new Rust project and create the HTTP server in main.rs. 
2. Add all dependencies to Cargo.toml.
3. Write Dockerfile to containerize the service
4. Build the Docker Image and create the Docker containerize
```
docker build -t week10 .
docker run -p 8080:8080 -d week10
```
5. Test the serverless endpoint locally 
```
curl -X POST http://localhost:8080/transformer -d '{"input":"Hello, World!"}' -H 'Content-Type: application/json'

```
6. Push Docker Image to Amazon ECR
- create new ECR repository
```
aws ecr create-repository --repository-name week10
```
- login to the ECR
```
aws ecr get-login-password --region <region> | docker login --username AWS --password-stdin <account-id>.dkr.ecr.<region>.amazonaws.com
```
- tag your docker image
```
docker tag <local-image>:<tag> <account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>:<tag>
```
- push your image to ECR repository
```
docker push <account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>:<tag>
```
7. Create Lambda Function with the Container Image. In the AWS Console, create a new Lambda function using existing container image that is pushed to ECR.

8. Create an API Gateway for the function and test it.

###  Screenshot

- Test the endpoint local using curl command.

![](pic/pic1.png)

- Push Docker Image to Amazon ECR.

![](pic/pic2.png)

- Docker Images in Amazon ECR repository.

![](pic/pic3.png)

- Deploy lambda function and create API Gateway.

![](pic/pic4.png)

